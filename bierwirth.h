#ifndef BIERWIRTH_H
#define BIERWIRTH_H

#include <iostream>
#include <vector>
#include "instance.h"

class Bierwirth
{
private:
    int n; //Nombre de Jobs
    int m; //Nombre de Machines
    std::vector<int> vecteur; //Vecteur de Bierwith
    std::vector<int> vecteurOptimal; //Vecteur de Bierwith optimal
    int makespan; //Valeur de fin du chemin critique
    int makespanOptimal; //Valeur de fin du chemin critique optimal

 public:
    Bierwirth();
    Bierwirth(int n,int m);
    int getN() const;
    void setN(int value);
    int getM() const;
    void setM(int value);
    std::vector<int> getVecteur() const;
    void setVecteur(const std::vector<int> &value);    
    int getMakespan() const;
    void setMakespan(int value);
    int getLastLigne() const;
    void setLastLigne(int value);
    int getLastColonne() const;
    void setLastColonne(int value);    
    int getMakespanOptimal() const;
    void setMakespanOptimal(int value);
    std::vector<int> getVecteurOptimal() const;
    void setVecteurOptimal(const std::vector<int> &value);

    //Evaluer le job shop
    void evaluate(Instance& instance);
    void afficherVecteur() const;
    void afficherVecteurOptimal() const;
    //Swap les éléments du vecteur en fonction du chemin critique
    void optimisationVecteur(Instance &instance);
};

#endif // BIERWIRTH_H
