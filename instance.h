#ifndef INSTANCE_H
#define INSTANCE_H
#include "operation.h"
#include <iostream>
#include <vector>
class Instance
{
private:
    std::string nom; //Nom du Job Shop
    int n; //Nombre de jobs
    int m; //Nombre de machines
    std::vector<std::vector<Operation>> matriceOperation;
    std::vector<int> cheminCritique;
    int lastLigne; //Ligne derniere op chemin critique
    int lastColonne; //Colonne derniere op chemin critique

public:
    Instance();
    Instance(std::string nom);
    std::string getNom() const;
    void setNom(const std::string &value);
    std::vector<std::vector<Operation>> getMatriceOperation() const;
    void setMatriceOperation(const std::vector<std::vector<Operation>> &value);    
    int getN() const;
    void setN(int value);
    int getM() const;
    void setM(int value);
    int getLastLigne() const;
    void setLastLigne(int value);
    int getLastColonne() const;
    void setLastColonne(int value);
    std::vector<int> getCheminCritique() const;
    void setCheminCritique(const std::vector<int> &value);

    //Créer la matrice du Job Shop grâce au fichier passé en paramètre
    void lireInstanceFromFichier(const std::string f);
    //Ajouter l'élément passé en paramètre au vecteur de chemin critique
    void pushCheminCritique(const int value);
    //Déterminer le chemin critique une fois l'intance évaluer grâce au vecteur de Bierwirth
    void determineCheminCritique();
    void afficher() const;
    /*Remet le chemin critique et les dates au plus tard à 0 pour
    la prochaine évaluation*/
    void remiseAZero();
};

#endif // INSTANCE_H
