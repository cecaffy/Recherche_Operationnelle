#include "operation.h"

#include <iostream>


//Constructeur
Operation::Operation(){}
Operation::Operation(int numMachine, int numJob, int duree):numMachine(numMachine),numJob(numJob),duree(duree)
{
    stop = 0;
    index = -1;
    ligne = -1;
    colonne = -1;
    predLigne = -1;
    predColonne = -1;
}

//Getter / Setter
int Operation::getIndex() const
{
    return index;
}

void Operation::setIndex(int value)
{
    index = value;
}

int Operation::getStop() const
{
    return stop;
}

void Operation::setStop(int value)
{
    stop = value;
}

int Operation::getPredLigne() const
{
    return predLigne;
}

void Operation::setPredLigne(int value)
{
    predLigne = value;
}

int Operation::getPredColonne() const
{
    return predColonne;
}

void Operation::setPredColonne(int value)
{
    predColonne = value;
}

int Operation::getLigne() const
{
    return ligne;
}

void Operation::setLigne(int value)
{
    ligne = value;
}

int Operation::getColonne() const
{
    return colonne;
}

void Operation::setColonne(int value)
{
    colonne = value;
}

int Operation::getNumMachine() const
{
    return numMachine;
}

void Operation::setNumMachine(int value)
{
    numMachine = value;
}

int Operation::getNumJob() const
{
    return numJob;
}

void Operation::setNumJob(int value)
{
    numJob = value;
}

int Operation::getDuree() const
{
    return duree;
}

void Operation::setDuree(int value)
{
    duree = value;
}

void Operation::afficher() const
{
    std::cout << index << " " << predLigne << " " << predColonne << " " << stop;
    std::cout << "   ";
}
