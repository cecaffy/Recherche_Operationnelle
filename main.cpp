#include <iostream>
#include "instance.h"
#include "operation.h"
#include "bierwirth.h"
#include <time.h>

int main(int, char**)
{
    srand(static_cast<unsigned int>(time(0)));
    Instance instance("la01.dat");
    instance.lireInstanceFromFichier("C:\\Users\\Lingfar\\Documents\\ISIMA\\RO\\TP2_RO\\INSTANCES\\la01.dat");

    Bierwirth bierwirth(instance.getN(), instance.getM());
    for(int i = 0; i < 2000; i++)
    {
        bierwirth.optimisationVecteur(instance);
    }
    bierwirth.afficherVecteurOptimal();
    std::cout << "Makespan = " << bierwirth.getMakespanOptimal() << std::endl;

    return 0;
}

