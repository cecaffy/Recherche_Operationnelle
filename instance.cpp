#include "instance.h"
#include <fstream>
#include <string>

//Constructeur
Instance::Instance()
{
    lastColonne = -1;
    lastLigne = -1;
}
Instance::Instance(std::string nom):nom(nom)
{
    lastColonne = -1;
    lastLigne = -1;
}

//Getter / Setter
int Instance::getN() const
{
    return n;
}

void Instance::setN(int value)
{
    n = value;
}

int Instance::getM() const
{
    return m;
}

void Instance::setM(int value)
{
    m = value;
}

std::vector<int> Instance::getCheminCritique() const
{
    return cheminCritique;
}

void Instance::setCheminCritique(const std::vector<int> &value)
{
    cheminCritique = value;
}

int Instance::getLastLigne() const
{
    return lastLigne;
}

void Instance::setLastLigne(int value)
{
    lastLigne = value;
}

int Instance::getLastColonne() const
{
    return lastColonne;
}

void Instance::setLastColonne(int value)
{
    lastColonne = value;
}

std::string Instance::getNom() const
{
    return nom;
}

void Instance::setNom(const std::string &value)
{
    nom = value;
}

std::vector<std::vector<Operation>> Instance::getMatriceOperation() const
{
    return matriceOperation;
}

void Instance::setMatriceOperation(const std::vector<std::vector<Operation>> &value)
{
    matriceOperation = value;
}

//Détermine le chemin critique de l'instance
void Instance::determineCheminCritique()
{
    int ligne = lastLigne;
    int colonne = lastColonne;
    int oldLigne = lastLigne;

    while(ligne > -1)
    {
        cheminCritique.push_back(matriceOperation[ligne][colonne].getIndex());
        ligne = matriceOperation[ligne][colonne].getPredLigne();
        colonne = matriceOperation[oldLigne][colonne].getPredColonne();
        oldLigne = ligne;
    }
}

//Ajouter l'élément passé en paramètre au vecteur de chemin critique
void Instance::pushCheminCritique(const int value)
{
    cheminCritique.push_back(value);
}

//Créer l'instance depuis le fichier passé en paramètre
void Instance::lireInstanceFromFichier(const std::string f)
{
    std::ifstream file(f);

    if(!file.is_open())
    {
        std::cerr << "Erreur : Impossible d'ouvrir " << f << std::endl;
        exit(1);
    }

    std::string poubelle;
    std::getline(file,poubelle);
    file >> n >> m;
    matriceOperation.resize(n);
    for(int i=0;i<n;i++){
        matriceOperation[i].resize(m);
    }
    for(int i = 0; i < n; i++)
    {
        for(int j=0;j<m;j++){
            int numMachine,duree;
            file >> numMachine >> duree;
            Operation op(numMachine,i,duree);
            matriceOperation[i][j]=op;
        }
    }

    file.close();
}

/*
 * Remet le chemin critique et les dates au plus tard à 0 pour
 * la prochaine évaluation
*/
void Instance::remiseAZero()
{
    cheminCritique.erase(cheminCritique.begin(), cheminCritique.end());
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            matriceOperation[i][j].setPredLigne(-1);
            matriceOperation[i][j].setPredColonne(-1);
            matriceOperation[i][j].setStop(0);
        }
    }
}

//Afficher la matrice de l'instance
void Instance::afficher() const
{
    for(int i = 0; i < getMatriceOperation().size(); i++)
    {
        for(int j = 0; j < getMatriceOperation()[0].size(); j++)
            getMatriceOperation()[i][j].afficher();        
        std::cout<<std::endl;
    }
}


