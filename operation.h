#ifndef OPERATION_H
#define OPERATION_H

class Operation
{
private:
    int numMachine; //Numéro de la machine correspondant
    int numJob; //Numéro du job correspondant
    int duree; //Durée de l'opération
    int ligne; //Ligne dans la matrice des opérations d'instance
    int colonne; //Colonne dans la matrice des opérations d'instance
    int index; //Index de l'élément du vecteur de Bierwith qui définit l'op (utilile pour recherche locale)
    int stop; //Date de fin au plus tôt
    int predLigne; //Ligne de l'op précédente
    int predColonne; //Colonne de l'op précédente;

public:
    Operation();
    Operation(int numMachine,int numJob,int duree);
    int getNumMachine() const;
    void setNumMachine(int value);
    int getNumJob() const;
    void setNumJob(int value);
    int getDuree() const;
    void setDuree(int value);
    void afficher() const;
    int getIndex() const;
    void setIndex(int value);
    int getStop() const;
    void setStop(int value);
    int getPredLigne() const;
    void setPredLigne(int value);
    int getPredColonne() const;
    void setPredColonne(int value);
    int getLigne() const;
    void setLigne(int value);
    int getColonne() const;
    void setColonne(int value);
};

#endif // OPERATION_H
