#include "bierwirth.h"
#include <algorithm>

//Constructeur
Bierwirth::Bierwirth()
{
    makespan = 0;
    makespanOptimal = INT_MAX;
}

Bierwirth::Bierwirth(int n,int m) : n(n), m(m)
{
    makespan = 0;
    vecteur.resize(n * m);
    int index = 0;
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            vecteur[index] = i;
            index++;
        }
    }
    std::random_shuffle(vecteur.begin(), vecteur.end());
}

//Getter / Setter
int Bierwirth::getN() const
{
    return n;
}

void Bierwirth::setN(int value)
{
    n = value;
}

int Bierwirth::getM() const
{
    return m;
}

void Bierwirth::setM(int value)
{
    m = value;
}

std::vector<int> Bierwirth::getVecteur() const
{
    return vecteur;
}

void Bierwirth::setVecteur(const std::vector<int> &value)
{
    vecteur = value;
}

int Bierwirth::getMakespan() const
{
    return makespan;
}

void Bierwirth::setMakespan(int value)
{
    makespan = value;
}

int Bierwirth::getMakespanOptimal() const
{
    return makespanOptimal;
}

void Bierwirth::setMakespanOptimal(int value)
{
    makespanOptimal = value;
}

std::vector<int> Bierwirth::getVecteurOptimal() const
{
    return vecteurOptimal;
}

void Bierwirth::setVecteurOptimal(const std::vector<int> &value)
{
    vecteurOptimal = value;
}


//Evalue la matrice d'instance passée en paramètre
void Bierwirth::evaluate(Instance& instance)
{
    instance.remiseAZero();
    makespan = 0;
    std::vector<std::vector<Operation>> &matrix = instance.getMatriceOperation();
    std::vector<int> tabLastJob = {0};
    tabLastJob.resize(n);
    std::vector<int> tabLastMachine = {0};
    tabLastMachine.resize(n);
    std::vector<Operation> tabLastOpMachine;
    tabLastOpMachine.resize(m);
    std::vector<Operation> tabLastOpJob;
    tabLastOpJob.resize(n);

    for(int i = 0; i < vecteur.size(); i++)
    {
        int jobId = vecteur[i];
        int j = 0;

        while(j < n && matrix[jobId][j].getStop() != 0)
            j++;

        Operation op = matrix[jobId][j];
        op.setIndex(i);
        op.setLigne(jobId);
        op.setColonne(j);
        int stop = std::max(tabLastJob[jobId], tabLastMachine[op.getNumMachine()]);
        op.setStop(stop + op.getDuree());

        if(tabLastJob[jobId] != 0 || tabLastMachine[op.getNumMachine()] != 0)
        {
            if(tabLastJob[jobId] >= tabLastMachine[op.getNumMachine()])
            {
                op.setPredColonne(tabLastOpJob[jobId].getColonne());
                op.setPredLigne(tabLastOpJob[jobId].getLigne());
            }
            else
            {
                op.setPredColonne(tabLastOpMachine[op.getNumMachine()].getColonne());
                op.setPredLigne(tabLastOpMachine[op.getNumMachine()].getLigne());
            }
        }

        matrix[jobId][j] = op;
        tabLastJob[jobId] = op.getStop();
        tabLastOpJob[jobId] = matrix[jobId][j];
        tabLastMachine[op.getNumMachine()] = op.getStop();
        tabLastOpMachine[op.getNumMachine()] = matrix[jobId][j];

        if(op.getStop() > makespan)
        {
            makespan = op.getStop();
            instance.setLastLigne(jobId);
            instance.setLastColonne(j);
        }
    }

    instance.setMatriceOperation(matrix);
    instance.determineCheminCritique();
}

/*
 * Optimise le vecteur de Bierwirth le plus possible en changeant 2 éléments
 * du vecteur de Bierwirth s'ils sont sur le chemin critique puis réévalue
 * avec ce nouveau vecteur ...
*/
void Bierwirth::optimisationVecteur(Instance &instance)
{
    std::random_shuffle(vecteur.begin(), vecteur.end());
    //afficherVecteur();

    std::vector<int> lastOp;
    lastOp.resize(instance.getN());

    int index = 0, index2 = 0;
    int l = 0;
    int c = 0;
    int temp = 0;
    int minMakespan = INT_MAX;
    bool eval = true;
    std::vector<int> &chemin = instance.getCheminCritique();

    while(index < vecteur.size())
    {
        int stop = 0;

        if(eval)
        {
            evaluate(instance);
            chemin = instance.getCheminCritique();
            eval = false;
        }

        if(makespan < minMakespan)
        {
            minMakespan = makespan;
            index = 0;
            index2 = 0;            

            if(makespan < makespanOptimal)
            {
                makespanOptimal = makespan;
                vecteurOptimal = vecteur;
            }
        }
        else
        {
            vecteur[index2] = vecteur[index];
            vecteur[index] = temp;
        }

        while(stop != 2 && index < vecteur.size())
        {
            l = vecteur[index];
            c = lastOp[l];

            if(std::find(chemin.begin(), chemin.end(), index) != chemin.end())
            {
                if(stop == 0)
                    index2 = index;
                stop++;
            }
            lastOp[l]++;
            index++;
        }

        if(stop == 2)
        {
            index--;
            temp = vecteur[index];
            vecteur[index] = vecteur[index2];
            vecteur[index2] = temp;
            eval = true;
        }
    }
}

void Bierwirth::afficherVecteur() const
{
    std::cout << "Vecteur = ";
    for(int i = 0; i < vecteur.size(); i++)
        std::cout << vecteur[i] << " ";
    std::cout << std::endl;
}

void Bierwirth::afficherVecteurOptimal() const
{
    std::cout << "Vecteur optimal = ";
    for(int i = 0; i < vecteurOptimal.size(); i++)
        std::cout << vecteurOptimal[i] << " ";
    std::cout << std::endl;
}
