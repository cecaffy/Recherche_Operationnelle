TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    operation.cpp \
    instance.cpp \
    bierwirth.cpp

HEADERS += \
    operation.h \
    instance.h \
    bierwirth.h

